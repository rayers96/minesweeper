//
//  ProjectSubmission.cpp
//  Minesweeper
//
//  Created by Ryan Ayers on 5/6/20.
//

#include <random>
#include <algorithm>
#include <iostream>
#include <iomanip>

using std::random_device;
using std::mt19937;
using std::shuffle;
using std::cout;
using std::endl;
using std::setw;
using std::setfill;
using std::cin;
using std::numeric_limits;
using std::streamsize;
using std::string;



/* Abstract Classes */



#ifndef Tile_hpp
#define Tile_hpp

class Tile {
protected:
  char disp;
  bool mine;
  bool flipped;
  bool flagged;
  int adj;
public:
  virtual char getDisp() = 0;
  virtual bool isMine() = 0;
  virtual void setMine(bool b) = 0;
  virtual bool isFlipped() = 0;
  virtual void setFlipped(bool b) = 0;
  virtual bool isFlagged() = 0;
  virtual void setFlagged(bool b) = 0;
  virtual int getAdj() = 0;
  virtual void incAdj(int i) = 0;
};

#endif /* Tile_hpp */

#ifndef Rules_hpp
#define Rules_hpp

class Rules {
protected:
  int w;
  int h;
  int m;
public:
  virtual bool validateXY(int x, int y) = 0;
  virtual int width() = 0;
  virtual int height() = 0;
  virtual int mines() = 0;
};

#endif /* Rules_hpp */

#ifndef Board_hpp
#define Board_hpp

class Board {
protected:
  Tile** b;
  Rules* r;
public:
  virtual ~Board() {}
  virtual Rules* rules() = 0;
  virtual Tile* getTile(int x, int y) = 0;
};

#endif /* Board_hpp */

#ifndef Generate_hpp
#define Generate_hpp

class Generate {
protected:
  random_device rd;
  mt19937 g;
  Board* b;
  Rules* r;
public:
  virtual ~Generate() {}
  virtual Board* createBoard(int lvl) = 0;
};

#endif /* Generate_hpp */



/* Square Classes */



#ifndef SquareTile_hpp
#define SquareTile_hpp

class SquareTile : public Tile {
  friend class SquareGenerate;
  friend class SquareBoard;
private:
  SquareTile();
public:
  char getDisp() override;
  bool isMine() override;
  void setMine(bool b) override;
  bool isFlipped() override;
  void setFlipped(bool b) override;
  bool isFlagged() override;
  void setFlagged(bool b) override;
  int getAdj() override;
  void incAdj(int i) override;
};

#endif /* SquareTile_hpp */

SquareTile::SquareTile() {
  disp = ' ';
  mine = false;
  flipped = false;
  flagged = false;
  adj = 0;
}
char SquareTile::getDisp() {return disp;}
bool SquareTile::isMine() {return mine;}
void SquareTile::setMine(bool b) {mine = b;}
bool SquareTile::isFlipped() {return flipped;}
void SquareTile::setFlipped(bool b) {
  flipped = b;
  if (b && adj == 0) {
    disp = '-';
  }
  else if (b && adj > 0) {
    disp = '0' + adj;
  }
}
bool SquareTile::isFlagged() {return flagged;}
void SquareTile::setFlagged(bool b) {
  flagged = b;
  if (b) {
    disp = '*';
  }
  else if (!b) {
    disp = ' ';
  }
}
int SquareTile::getAdj() {return adj;}
void SquareTile::incAdj(int i) {adj += i;}

#ifndef SquareRules_hpp
#define SquareRules_hpp

class SquareRules : public Rules {
  friend class SquareGenerate;
private:
  SquareRules(int lvl);
public:
  bool validateXY(int x, int y) override;
  int width() override;
  int height() override;
  int mines() override;
};

#endif /* SquareRules_hpp */

SquareRules::SquareRules(int lvl) {
  if (lvl == 1) {
    w = 7;
    h = 7;
    m = 10;
  } else if (lvl == 2) {
    w = 10;
    h = 10;
    m = 20;
  } else if (lvl == 3) {
    w = 13;
    h = 13;
    m = 35;
  }
}
bool SquareRules::validateXY(int x, int y) {
  if (x >= 0 && y >= 0) {
    if (x < w && y < h) {
      return true;
    }
  }
  return false;
}
int SquareRules::width() {return w;}
int SquareRules::height() {return h;}
int SquareRules::mines() {return m;}

#ifndef SquareBoard_hpp
#define SquareBoard_hpp

class SquareBoard : public Board {
  friend class SquareGenerate;
private:
  SquareBoard(Rules* r);
public:
  ~SquareBoard() override;
  Rules* rules() override;
  Tile* getTile(int x, int y) override;
};
#endif /* SquareBoard_hpp */

SquareBoard::SquareBoard(Rules* r) {
  this->r = r;
  // Create empty board
  b = new Tile*[r->width()];
  for (int i = 0; i < r->width(); i++) {
    b[i] = new SquareTile[r->height()];
  }
}
SquareBoard::~SquareBoard() {
  delete b;
}
Rules* SquareBoard::rules() {return r;}
Tile* SquareBoard::getTile(int x, int y) {return &b[x][y];}

#ifndef SquareGenerate_hpp
#define SquareGenerate_hpp

class SquareGenerate : public Generate {
private:
  void genMines();
  void incAdjacent(int x, int y);
public:
  SquareGenerate();
  ~SquareGenerate() override;
  Board* createBoard(int lvl) override;
};

#endif /* SquareGenerate_hpp */

SquareGenerate::SquareGenerate() {
  g = mt19937(rd());
}
SquareGenerate::~SquareGenerate() {
}
// Generate an int index for each of the mines
Board* SquareGenerate::createBoard(int lvl) {
  r = new SquareRules(lvl);
  b = new SquareBoard(r);
  // Generate mine coords and incAdjacent tiles
  genMines();
  return b;
}
/* Helper Functions */
void SquareGenerate::genMines() {
  int* a = new int[r->width()*r->width()];
  for (int i = 0; i < r->width()*r->width(); i++) {
    a[i] = i;
  }
  shuffle(&a[0], &a[r->width()*r->width()], g);
  for (int i = 0; i < r->mines(); i++) {
    int x = a[i] % r->width();
    int y = a[i] / r->height();
    b->getTile(x,y)->setMine(true);
    // Increment Adjacent Tiles
    incAdjacent(x, y);
  }
  delete[] a;
}
void SquareGenerate::incAdjacent(int x, int y) {
  for (int i = x - 1; i <= x + 1; i++) {
    for (int j = y - 1; j <= y + 1; j++) {
      if (i >= 0 && i < r->width() && j >= 0 && j < r->height()) {
        if (i != x || y != j) {
          Tile* t = b->getTile(i, j);
          t->incAdj(1);
        }
      }
    }
  }
}



/* Client */



#ifndef Minesweeper_hpp
#define Minesweeper_hpp

class Minesweeper {
private:
  Generate* g;
  Board* b;
  Rules* r;
  int flipped;
  int flagged;
  void lose();
  void win();
  void flipAdj(int x, int y);
  void print(bool revealed);
public:
  Minesweeper(int level); // 1,2,3 - Levels are defined in Rules
  ~Minesweeper();
  // Return 0 for invalid input
  bool flip(int x, int y);
  bool flag(int x, int y);
  bool unflag(int x, int y);
};

#endif /* Minesweeper_hpp */

/* Client Interface */
Minesweeper::Minesweeper(int level) {
  g = new SquareGenerate();
  b = g->createBoard(level);
  r = b->rules();
  flipped = 0;
  flagged = 0;
  print(false);
  delete g;
}
Minesweeper::~Minesweeper() {
  delete b;
}
bool Minesweeper::flip(int x, int y) {
  if (!r->validateXY(x, y)) {
    cout<<"Coordinates out of bounds!"<<endl;
    return false;
  }
  Tile* t = b->getTile(x, y);
  if (t->isFlipped()) {
    cout<<"Tile already flipped!"<<endl;
  } else if (t->isFlagged()) {
    cout<<"Tile is flagged! Unflag to flip."<<endl;
  } else if (t->isMine()) {
    lose();
  } else  {
    t->setFlipped(true);
    flipped++;
    int unflipped = (r->width() * r->height()) - flipped;
    if (unflipped == r->mines()) win();
    else if (t->getAdj() == 0) flipAdj(x,y);
    print(false);
    return true;
  }
  return false;
}
bool Minesweeper::flag(int x, int y) {
  if (!r->validateXY(x, y)) {
    cout<<"Coordinates out of bounds!"<<endl;
    return false;
  }
  Tile* t = b->getTile(x, y);
  if (t->isFlipped()) {
    cout<<"Tile is flipped; cannot be flagged/unflagged."<<endl;
  } else if (t->isFlagged()) {
    cout<<"Tile already flagged!"<<endl;
  } else if (flagged == r->mines()) {
    cout<<"Max number of flags reached!"<<endl;
  } else {
    t->setFlagged(true);
    flagged++;
    print(false);
    return true;
  }
  return false;
}
bool Minesweeper::unflag(int x, int y) {
  if (!r->validateXY(x, y)) {
    cout<<"Coordinates out of bounds!"<<endl;
    return false;
  }
  Tile* t = b->getTile(x, y);
  if (t->isFlipped()) {
    cout<<"Tile is flipped; cannot be flagged/unflagged."<<endl;
  } else if (!t->isFlagged()) {
    cout<<"Tile not flagged!"<<endl;
  } else {
    t->setFlagged(false);
    flagged--;
    print(false);
    return true;
  }
  return false;
}
/* Helper Functions */
void Minesweeper::lose() {
  print(true);
  cout<<"Game over!"<<endl;
  exit(0);
}
void Minesweeper::win() {
  print(true);
  cout<<"You win!"<<endl;
  exit(0);
}
void Minesweeper::flipAdj(int x, int y) {
  // Assumes 8 adj Tiles
  for (int i = x - 1; i <= x + 1; i++) {
    for (int j = y - 1; j <= y + 1; j++) {
      if (i >= 0 && i < r->width() && j >= 0 && j < r->height()) {
        if (i != x || y != j) {
          Tile* t = b->getTile(i, j);
          if (t->isFlipped()) {
            continue;
          } else {
            t->setFlipped(true);
            flipped++;
            if (t->getAdj() == 0) flipAdj(i, j);
          }
        }
      }
    }
  }
}
void Minesweeper::print(bool revealed) {
  // X Label
  cout<<"   ";
  for (int i = 0; i < r->width(); i++) {
    cout<<setw(2)<<setfill(' ')<<i;;
    cout<<" ";
  }
  cout<<"(X)"<<endl;
  // Board & Y Label
  for (int j = 0; j < r->width(); j++) {
    cout<<setw(2)<<setfill(' ')<<j;;
    cout<<" ";
    for (int i = 0; i < r->height(); i++) {
      cout<<"[";
      if (revealed && b->getTile(i, j)->isMine()) {
        cout<<"X";
      } else {
        cout<<b->getTile(i, j)->getDisp();
      }
      cout<<"]";
    }
    cout<<endl;
  }
  cout<<"(Y)   ";
  cout<<"Flags remaining: "<<r->mines() - flagged<<endl<<endl;
}



/* main */



int main(int argc, const char * argv[]) {
  cout<<"Welcome to Minesweeper!"<<endl;
  int lvl;
  for (;;) {
    cout<<"Choose a level (1, 2, 3): ";
    if (cin>>lvl && lvl >= 1 && lvl <= 3) {
      break;
    } else {
      cout<<"Invalid input!"<<endl;
      cin.clear();
      cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
  }
  cout<<endl;
  Minesweeper m = Minesweeper(lvl);
  string s;
  int x;
  int y;
  for (;;) {
    cout<<"Commands: flip, flag, unflag (e.g. flip x y)"<<endl;
    cout<<"Enter a command: ";
    if (cin>>s && cin>>x && cin>>y) {
      if (s == "flip") m.flip(x,y);
      else if (s == "flag") m.flag(x,y);
      else if (s == "unflag") m.unflag(x,y);
      else cout<<"Unrecognized Command!"<<endl;
      continue;
    } else {
      cout<<"Invalid Input!"<<endl;
      cin.clear();
      cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
  }
return 0;
}
