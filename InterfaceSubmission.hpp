//
//  InterfaceSubmission.hpp
//  Minesweeper
//
//  Created by Ryan Ayers on 4/30/20.
//

#include <random>

/* Virtul Classes */

class Tile {
public:
  char disp;
  bool mine;
  bool flipped;
  bool flagged;
  int adj;
};
class Rules {
public:
  const int width;
  const int height;
  const int mines;
};
class Generate {
private:
  std::random_device rd;
  std::mt19937 g;
  std::uniform_int_distribution<> d;
public:
  virtual ~Generate() {}
  virtual int gen() = 0;
};
class Board {
public:
  Tile** b;
  Rules* r;
  virtual ~Board() {}
};
class Factory {
private:
  Board* b;
  Rules* r;
  Generate* g;
public:
  virtual ~Factory() {}
  virtual Board* createBoard(int lvl) = 0;
};

/* End Virtual Classes */

/* Square Classes */

class SquareTile : Tile {
public:
  char disp;
  bool mine;
  bool flipped;
  bool flagged;
  int adj;
  SquareTile(bool m);
};
class SquareRules : Rules {
public:
  const int width;
  const int height;
  const int mines;
  SquareRules(int lvl);
};
class SquareGenerate : Generate {
private:
  std::random_device rd;
  std::mt19937 g;
  std::uniform_int_distribution<> d;
public:
  SquareGenerate();
  ~SquareGenerate();
  int gen();
};
class SquareBoard : Board {
public:
  Tile** b;
  Rules* r;
  SquareBoard(Rules* r);
  ~SquareBoard();
};
class SquareFactory : Factory {
private:
  Board* b;
  Generate* g;
  Rules* r;
public:
  SquareFactory();
  ~SquareFactory();
  Board* createBoard(int lvl);
};

/* End Square Classes */

/* Client */

class Minesweeper {
private:
  Factory* f;
  Board* b;
public:
  Minesweeper(int level); // 1,2,3 - Levels are defined in Rules
  ~Minesweeper();
  // Return 0 for invalid input
  bool flag(int x, int y);
  bool flip(int x, int y);
  bool unflag(int x, int y);
};
